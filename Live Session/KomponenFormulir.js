export const KomponenFormulir = {
  template: `
    <form @submit.prevent="kirimFormulir">
      <div v-if="erorEror.length">
        <p><i>Mohon benarkan kesalahan berikut:</i></p>
        <ul>
          <li v-for="eror in erorEror"><i>{{ eror }}</i></li>
        </ul>
      </div>

      <label for="nama-anggota">Nama:</label>
      <br>
      <input type="text" id="nama-anggota" v-model="nama" ref="nama" :disabled="mode == 'unggah'">
      <br>
      <label for="alamat-anggota">Alamat:</label>
      <br>
      <textarea id="alamat-anggota" v-model="alamat" ref="alamat" :disabled="mode == 'unggah'"></textarea>
      <br>
      <label for="nomor-ponsel">Nomor Ponsel:</label>
      <br>
      <input type="tel" id="nomor-ponsel" v-model="nomorPonsel" ref="nomorPonsel" :disabled="mode == 'unggah'">
      <br><br>

      <div v-if="mode == 'daftar'">
        <input type="submit" value="Kirim">
      </div>
      <div v-else-if="mode == 'sunting'">
        <input type="button" value="Mutakhirkan" @click="mutakhirkanAnggota">
        <input type="button" value="Batal" @click="aturUlangFormulir">
      </div>
      <div v-else-if="mode == 'unggah'">
        <input type="file" ref="foto">
        <br><br>
        <input type="button" value="Unggah" @click="kirimFoto">
        <input type="button" value="Batal" @click="aturUlangFormulir">
      </div>
    </form>
  `,
  computed: {
    erorEror() {
      return this.$store.state.erorEror
    },
    mode() {
      return this.$store.state.mode
    },
    nama: {
      get() {
        return this.$store.state.nama
      },
      set(nilai) {
        this.$store.commit('aturNama', nilai)
      },
    },
    alamat: {
      get() {
        return this.$store.state.alamat
      },
      set(nilai) {
        this.$store.commit('aturAlamat', nilai)
      },
    },
    nomorPonsel: {
      get() {
        return this.$store.state.nomorPonsel
      },
      set(nilai) {
        this.$store.commit('aturNomorPonsel', nilai)
      },
    },
  },
  methods: {
    kirimFormulir() {
      this.$store.dispatch('kirimFormulir')
    },
    validasiFormulir() {
      this.$store.commit('validasiFormulir')
    },
    aturUlangFormulir() {
      this.$store.commit('aturUlangFormulir')
    },
    mutakhirkanAnggota() {
      this.$store.dispatch('mutakhirkanAnggota')
    },
    kirimFoto() {
      const fail = this.$refs.foto.files[0]

      this.$store.dispatch('kirimFoto', fail)
    },
  },
}
