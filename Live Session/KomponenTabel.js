export const KomponenTabel = {
  template: `
    <table border="1">
      <tr>
        <th>Foto</th>
        <th>Nama</th>
        <th>Alamat</th>
        <th>Nomor Ponsel</th>
        <th>🔥</th>
      </tr>
      <tr v-for="anggota of anggotaAnggota">
        <td>
          <img :src="anggota.photo_profile ? urlDasar + anggota.photo_profile : gambarSulih" :alt="anggota.nama" width="100" height="100">
        </td>
        <td>{{ anggota.name }}</td>
        <td>{{ anggota.address }}</td>
        <td>{{ anggota.no_hp }}</td>
        <td>
          <button @click="modeSunting(anggota)">Sunting …</button>
          <br><br>
          <button @click="modeUnggah(anggota)">Unggah Foto</button>
          <br><br>
          <button @click="hapusAnggota(anggota.id)">Hapus(!)</button>
          <br><br>
        </td>
      </tr>
    </table>
  `,
  computed: {
    anggotaAnggota() {
      return this.$store.state.anggotaAnggota
    },
    urlDasar() {
      return this.$store.state.urlDasar
    },
    gambarSulih() {
      return this.$store.state.gambarSulih
    },
  },
  methods: {
    modeSunting(anggota) {
      this.$parent.$children[0].$refs.nama.focus()

      this.$store.commit('modeSunting', anggota)
    },
    modeUnggah(anggota) {
      this.$store.commit('modeUnggah', anggota)
    },
    hapusAnggota(id) {
      this.$store.dispatch('hapusAnggota', id)
    },
  },
}
