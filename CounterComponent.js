export const CounterComponent = {
  template: `
    <div>
      <p><i>State pada komponen Counter 👉 {{ counter }}</i></p>
    </div>
  `,
  computed: {
    counter() {
      return this.$store.getters.counter
    },
  },
}
